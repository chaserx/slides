FROM pandoc/core
ADD . /data
COPY . /data
RUN ["apk", "add", "--no-cache", "bash"]
RUN ["chmod", "+x", "/data/convert.sh"]
ENTRYPOINT ["bash", "/data/convert.sh"]
