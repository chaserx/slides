#!/bin/bash

INPUT_FOLDER=markdown
files=($(find ${INPUT_FOLDER} -type f -name '*.md'))
for path in ${files[*]}
do
	echo "Processing $path"
  file=${path##*/}
  echo $file
  basename=${file%.*}
  echo "converting $file into html/$basename.html"
  pandoc --standalone -t revealjs -o "html/$basename.html" "$path"
done

echo "copying over all of reveal.js into the html directory"
cp -R reveal.js html
