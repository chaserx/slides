# slides

![](./slides_icon.png)
Slide Decks (using Reveal.js)

## Reveal.jS

This repo uses [Git Subtrees](https://docs.gitlab.com/ee/university/training/topics/subtree.html) to include Reveal.js as a dependency here.

You'll may need to install Node packages for Reveal.js

`cd reveal.js && npm install`

## Docker and Pandoc

This project uses Docker to run Pandoc from a container to convert Markdown to RevealJS compatible HTML via an entrypoint, convert.sh, file. If you've got Pandoc installed locally, you still probably use convert.sh to process the files in the Markdown directory.

1. `docker build -t slides .`
1. `` docker run --rm -v `pwd`:/data --name slides slides ``

## GitLab CI

This project uses [GitLab CI](https://docs.gitlab.com/ee/ci/examples/README.html) to convert the original Markdown files into Reveal.js HTML in a very similar fashion to that of the local Docker environment. The [CI script](https://gitlab.com/chaserx/slides/-/blob/master/.gitlab-ci.yml) then deploys HTML artifact to [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/). There might be a way to use the same Docker container locally in the GitLab Runner.

## Themes

You can update the theme by specifying the associated [theme name](https://revealjs.com/#/themes) in front matter of the markdown. There's probably a way to add custom themes.

## References

- [Another Subtree Cheatsheet](https://github.github.com/training-kit/downloads/submodule-vs-subtree-cheat-sheet/)
- [Pandoc Docker Images](https://github.com/pandoc/dockerfiles#available-images)
- [Pandoc and Reveal.js](https://github.com/jgm/pandoc/wiki/Using-pandoc-to-produce-reveal.js-slides)
- [Docker builder pattern](https://matthiasnoback.nl/2017/04/docker-build-patterns/)
- [Dockerfile best practices](https://docs.docker.com/develop/develop-images/dockerfile_best-practices/)
- [GitLab CI Reference](https://gitlab.com/help/ci/yaml/README)

## Attribution

Heavily inspired by this [Slide Decks](https://gitlab.com/benjifisher/slide-decks) repository.
